const H: ReadonlyArray<number> = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102]
Object.freeze(H)
const DASH:number = 0x2D // '-'.codePointAt(0)
const UUIDtoString = (
	bv: ArrayBufferView | ArrayBuffer | SharedArrayBuffer,
	offset: number = 0
):string  => {
	const b = ArrayBuffer.isView(bv)
		? new Uint8Array(bv.buffer, bv.byteOffset + offset, 16)
		: new Uint8Array(bv, offset, 16)
	return String.fromCodePoint(
		// 8-4-4-4-12
		H[b[0] >>> 4], H[b[0] & 15], H[b[1] >>> 4], H[b[1] & 15],
		H[b[2] >>> 4], H[b[2] & 15], H[b[3] >>> 4], H[b[3] & 15],
		0x2D,
		H[b[4] >>> 4], H[b[4] & 15], H[b[5] >>> 4], H[b[5] & 15],
		0x2D,
		H[b[6] >>> 4], H[b[6] & 15], H[b[7] >>> 4], H[b[7] & 15],
		0x2D,
		H[b[8] >>> 4], H[b[8] & 15], H[b[9] >>> 4], H[b[9] & 15],
		0x2D,
		H[b[10]>>> 4], H[b[10]& 15], H[b[11]>>> 4], H[b[11]& 15],
		H[b[12]>>> 4], H[b[12]& 15], H[b[13]>>> 4], H[b[13]& 15],
		H[b[14]>>> 4], H[b[14]& 15], H[b[15]>>> 4], H[b[15]& 15],
	)
}
interface uuidgenv4 {
	(): string;
	(buf: ArrayBufferView | ArrayBuffer |
		SharedArrayBuffer | undefined): string;
	(buf: ArrayBufferView | ArrayBuffer |
		SharedArrayBuffer | undefined, rStr?: boolean): string;
	(buf: ArrayBufferView | ArrayBuffer |
		SharedArrayBuffer | undefined, rStr: false): Uint8Array;
	(buf: ArrayBufferView | ArrayBuffer |
		SharedArrayBuffer | undefined, rStr?: boolean, offset?: number): string;
	(buf: ArrayBufferView | ArrayBuffer |
		SharedArrayBuffer | undefined, rStr: false, offset?: number): Uint8Array;
}
const uuidgenv4: uuidgenv4 = (
	buf: ArrayBufferView | ArrayBuffer | SharedArrayBuffer = new Uint8Array(16),
	rStr: boolean = true,
	offset: number = 0
):any => {
	const rng = crypto.getRandomValues(
		ArrayBuffer.isView(buf)
		? new DataView(buf.buffer, buf.byteOffset + offset, 16)
		: new DataView(buf, offset, 16)
	)
	if (rng === null) throw new TypeError('crypto.getRandomValues returned a null value')
	const rnd: Uint8Array = new Uint8Array(rng.buffer, rng.byteOffset, 16)
	rnd[6] = (rnd[6] & 15) | 64
	rnd[8] = (rnd[8] & 63) | 128
	if (rStr) return UUIDtoString(rnd)
	else return rnd
}


const O: {[oct: string]: number} = {
	0: 0, 1: 1, 2: 2, 3: 3,
	4: 4, 5: 5, 6: 6, 7: 7,
	8: 8, 9: 9, a: 10, b: 11,
	c: 12, d: 13, e: 14, f: 15,
	A: 10, B: 11, C: 12, D: 13,
	E: 14, F: 15
}
const stringToUUID = (
	s: string,
	buf: ArrayBufferView | ArrayBuffer | SharedArrayBuffer = new Uint8Array(16),
	offset: number = 0
): Uint8Array => {
	const v = ArrayBuffer.isView(buf)
		? new Uint8Array(buf.buffer, buf.byteOffset + offset, 16)
		: new Uint8Array(buf, offset, 16)
	// 8-4-4-4-12
	v[0 ] = O[s[0 ]] << 4 | O[s[1 ]]
	v[1 ] = O[s[2 ]] << 4 | O[s[3 ]]
	v[2 ] = O[s[4 ]] << 4 | O[s[5 ]]
	v[3 ] = O[s[6 ]] << 4 | O[s[7 ]]
	// 4-4-4-12
	v[4 ] = O[s[9 ]] << 4 | O[s[10]]
	v[5 ] = O[s[11]] << 4 | O[s[12]]
	// 4-4-12
	v[6 ] = O[s[14]] << 4 | O[s[15]]
	v[7 ] = O[s[16]] << 4 | O[s[17]]
	// 4-12
	v[8 ] = O[s[19]] << 4 | O[s[20]]
	v[9 ] = O[s[21]] << 4 | O[s[22]]
	// 12
	v[10] = O[s[24]] << 4 | O[s[25]]
	v[11] = O[s[26]] << 4 | O[s[27]]
	v[12] = O[s[28]] << 4 | O[s[29]]
	v[13] = O[s[30]] << 4 | O[s[31]]
	v[14] = O[s[32]] << 4 | O[s[33]]
	v[15] = O[s[34]] << 4 | O[s[35]]
	// bloody done
	return v
}
export default uuidgenv4
export {
	uuidgenv4,
	uuidgenv4 as uuid,
	uuidgenv4 as uuidv4,
	uuidgenv4 as uuidgen,
	uuidgenv4 as generate,
	uuidgenv4 as genuuid,
	uuidgenv4 as UUIDGEN,
	uuidgenv4 as UUID,
	uuidgenv4 as UUIDv4,

	H,
	H as h,
	H as hex,
	H as hexBytes,

	DASH,
	DASH as HYPHEN_DASH,

	UUIDtoString,
	UUIDtoString as BUF_UUID,

	stringToUUID,
	stringToUUID as UUID_BUF,
	stringToUUID as StringToUUID,
}

