import { UUID, UUIDtoString, stringToUUID } from './uuidgen4-node'
import { timingSafeEqual } from 'crypto'
function test(pass: boolean, title: string) {
	console.count('Test')
	console.assert(pass, title)
	console.log(`\t${title} \u{2714}`)
}
async function tests() {
	test(
		'string' === typeof await UUID(),
		'uuidgen4 returns strings on empty calls'
	)
	let nil = Buffer.alloc(16)
	test(
		'00000000-0000-0000-0000-000000000000' === UUIDtoString(nil),
		'UUIDtoString returns a Nil UUID on uinitialised calls'
	)
	test(
		await UUID(undefined, false) instanceof Uint8Array,
		'Disabling string returns a Uint8Array'
	)
	test(
		await UUID(
			new Uint32Array(nil.buffer, nil.byteOffset, 4),
			false
		) instanceof Uint8Array,
		'uuidgen4 accepts any TypedArray and returns a Uint8Array when rStr is false'
	)
	test(
		'string' === typeof await UUID(
			new Uint32Array(nil.buffer, nil.byteOffset, 4),
			true
		),
		'uuidgen4 accepts any TypedArray and returns a string when rStr is true'
	)
	let uuu = UUIDtoString(nil)
	let www = Buffer.from([...stringToUUID(uuu)])
	test(
		timingSafeEqual(nil, www),
		'stringToUUID returns a byte-equivalent Uint8Array'
	)
	test(
		ArrayBuffer.isView(await UUID(www.buffer, false, www.byteOffset)),
		'UUID() accepts an ArrayBuffer'
	)
	test(
		(await UUID(www.buffer, false, www.byteOffset)).buffer === www.buffer,
		'Passing an ArrayBuffer reuses the same ArrayBuffer'
	)
}

tests().catch(err => (console.error(err), Promise.reject(err)))
