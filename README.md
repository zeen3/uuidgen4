# uuidgenv4

Generates UUIDv4 (122 bit random) instances.

Usage:

```js
import { UUID, UUIDsync, UUIDtoString, StringToUUID } from 'uuidgenv4'

UUID().then(uuid => {
	// do stuff like db keys and so on
})

UUID(Buffer.alloc(16), false).then(buf => {
	// it's a 16 byteLength Uint8Array instance. write it to disk or something
})

let b = Buffer.alloc(36)
b.write('IAmYourFather', 0, 20, 'ascii')

UUID(b, false, 20).then(buf => {
	// also a 16 byteLength Uint8Array instance. Shares memory with b.
})

let key = UUIDsync()
console.log(key)
// 8-4-4-4-12
console.log(StringToUUID(key))
// Uint8Array [...]
```

